from cih.report import Report
from cih.section_1 import Section1
from cih.section_2 import Section2
from cih.section_3 import Section3
from cih.section_4 import Section4
from cih.section_5 import Section5
from cih.section_6 import Section6
from cih.section_7 import Section7
from cih.section_8 import Section8
from decouple import config
import datapane as dp

class ReportCGI():

    def __init__(self, organization_id):
        self.report = Report()
        self.organization_id = organization_id
        self.sections = []
        self.sections.append (Section1(organization_id=organization_id))
        self.sections.append (Section2(organization_id=organization_id))
        self.sections.append (Section3(organization_id=organization_id))
        self.sections.append (Section4(organization_id=organization_id))
        self.sections.append (Section5(organization_id=organization_id))
        self.sections.append (Section6(organization_id=organization_id))
        self.sections.append (Section7(organization_id=organization_id))
        self.sections.append (Section8(organization_id=organization_id))

    def create_report(self):
        for section in self.sections:    
            self.report.append_section(section.create())
        
        dp.Report(blocks=self.report.block_list).save(path='/app/report/report-{}.html'.format(self.organization_id), open=True)

   