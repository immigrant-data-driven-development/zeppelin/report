# Lib to support gap analysis
#Tutoriais
# https://www.datacamp.com/community/tutorials/histograms-matplotlib?utm_source=adwords_ppc&utm_campaignid=1455363063&utm_adgroupid=65083631748&utm_device=c&utm_keyword=&utm_matchtype=b&utm_network=g&utm_adpostion=&utm_creative=332602034358&utm_targetid=dsa-429603003980&utm_loc_interest_ms=&utm_loc_physical_ms=1001547&gclid=CjwKCAjwnK36BRBVEiwAsMT8WFpQ8Vnhx1pxEpNIA4JNvbohkL0g9QgmdRq3cgNBAqNVbDN3uCsy7BoCRYsQAvD_BwE

# dataSet
## https://data36.com/pandas-tutorial-2-aggregation-and-grouping/

from decouple import config
from sqlalchemy import create_engine
import pandas as pd 

class ReportRepository():

    def __init__(self):
        
        DB_HOST = config('DB_HOST')
        DB_USERNAME = config('DB_USERNAME')
        DB_PASSWORD = config('DB_PASSWORD')
        DB_PORT = config('DB_PORT')
        DB_NAME = config('DB_NAME')

        str_connection = '''postgresql://{}:{}@{}:{}/{}'''.format(DB_USERNAME,DB_PASSWORD,DB_HOST,DB_PORT,DB_NAME)
      
        self.cnx = create_engine (str_connection)

        self.__queries()
    
        
    def __queries(self):
        
        ''' Queries used in a report'''


        ###### NOVOS

        self.query_percentage_sth_organization= "Select sth_stage, trunc(SUM (value*adopted_level_percentage)/SUM(value),2) as percentage from  answer_sth_stage_adopted_level_view where organization_id = {} group by sth_stage order by sth_stage"
        self.query_percentage_eyes_organization = "select  eyes_dimension as dimension,trunc((SUM (practices_by_dimension*adopted_level_percentage)/SUM(practices_by_dimension)),2) as value from answer_sth_stage_eye_dimensions_adopted_level_view where organization_id = {} group by eyes_dimension order by value desc, eyes_dimension"
        self.query_percentage_eyes_sth_organization = "Select dimension, sth_stage, trunc( (sum (value*adopted_level_percentage)/sum(value)),2) as value from answer_eye_cse_dimension_adopted_level_view x where organization_id = {} group by sth_stage_id, sth_stage, dimension order by sth_stage_id, sth_stage, dimension"
        self.query_percentage_elements_organization = "select eyes_element as element, trunc(avg(practices_by_dimension*adopted_level_percentage)/sum(practices_by_dimension),2) as percentage from answer_sth_stage_eye_element_view where organization_id = {} group by eyes_element order by percentage desc, element"
        self.query_percentage_process_organization = "select process, trunc((SUM (practices_by_dimension*adopted_level_percentage)/SUM(practices_by_dimension)),2) as percentage from answer_sth_stage_fce_process_view where organization_id = {} group by  process order by percentage desc, process "

        self.query_percentage_elements_sth_organization = "Select  sth_stage,  element, trunc((sum (value*adopted_level_percentage)/sum(value)),2) as percentage from answer_eye_cse_element_adopted_level_view x where organization_id = {} group by element,  element_id, sth_stage_id, sth_stage order by element,  element_id, sth_stage_id,  sth_stage "
     
        self.query_percentage_sth_process_organization = "select process, sth_stage, trunc((SUM (practices_by_dimension*adopted_level_percentage)/SUM(practices_by_dimension)),2) as percentage from answer_sth_stage_fce_process_view where organization_id = {} group by process, sth_stage order by percentage desc, sth_stage, process "


        #####

        self.query_organization_process_count = "select distinct process, sth_stage, (count (*) * adopted_level_percentage) as qtd from  questionnaire_answer_view qav where organization_id = {} group by process, sth_stage, adopted_level_percentage order by qtd desc, process"

        self.query_organization_dimension_count = "select distinct sth_stage, category, (count (*) * adopted_level_percentage) as qtd from  questionnaire_answer_view qav  where organization_id = {}  group by category, sth_stage,adopted_level_percentage order by category"

        self.query_organization_dimension_element_count = "select distinct category, element, (count (*) * adopted_level_percentage) as qtd from questionnaire_answer_view qav where organization_id = {} group by category,element, adopted_level_percentage order by category"
        
        self.query_organization_sth_count = "select distinct  sth_stage, (count (*) * adopted_level_percentage) as qtd from  questionnaire_answer_view qav  where organization_id = {} group by sth_stage, adopted_level_percentage order by sth_stage"

        self.query_organization = "select distinct organization_name from questionnaire_answer_view where organization_id  = {}"

        self.query_sth_from_organization = "select distinct code, adopted_level_name, sth_stage, adopted_level_percentage from questionnaire_answer_view where organization_id = {} order by sth_stage"

        self.query_category_element_organization = "select distinct code, adopted_level_name, sth_stage, category, element , adopted_level_percentage from questionnaire_answer_view where organization_id = {} order by sth_stage"

        self.query_process_organization = "select * from questionnaire_answer_view where organization_id = {} order by sth_stage"

        self.query_category_sth = "select distinct code, element, category, sth_stage from assertive_category_stage_process order by category"
       
        self.query_adopted_level = "select name, description from questionnaire_adoptedlevel qa order by percentage "

        self.query_sth = "select name, description from sth_stage ss order by name"

        self.query_dimension_eye = "select name, description from practitioners_eye_dimension ped order by name"

        self.query_element_eye = "select  ped.name, string_agg (pee.name, ', ') as elements from practitioners_eye_element pee inner join practitioners_eye_dimension ped on pee.dimension_id = ped.id group by ped.name order by ped.name"

        self.query_process_fcse = "select distinct name, description from cse_framework_process cfp order by name"

        self.query_answers = "select distinct code, adopted_level_name, statement , answer_comment  from questionnaire_answer_view where organization_id  = {} order by code"

    def __execute_query(self, query):
        ''' Function what execute query using pandas and SQLAlquey'''
        return pd.read_sql_query (query, self.cnx)
    ###############################
    def get_query_percentage_sth_organization(self, organization_id):
        return self.__execute_query(self.query_percentage_sth_organization.format(organization_id))
    
    def get_query_percentage_eyes_organization(self, organization_id):
        return self.__execute_query(self.query_percentage_eyes_organization.format(organization_id))
    
    def get_query_percentage_eyes_sth_organization(self, organization_id):
        return self.__execute_query(self.query_percentage_eyes_sth_organization.format(organization_id))
    
    def get_query_percentage_elements_organization(self, organization_id):
        return self.__execute_query(self.query_percentage_elements_organization.format(organization_id))
    
    def get_query_percentage_elements_sth_organization(self,organization_id):
        return self.__execute_query(self.query_percentage_elements_sth_organization.format(organization_id))
    
    def get_query_percentage_process_organization(self, organization_id):
        return self.__execute_query(self.query_percentage_process_organization.format(organization_id))
    
    def get_query_percentage_sth_process_organization(self,organization_id):
        return self.__execute_query(self.query_percentage_sth_process_organization.format(organization_id))
    
    
    ########################################
    def get_query_organization_process_count(self, organization_id):
        return self.__execute_query(self.query_organization_process_count.format(organization_id))
    
    def get_query_organization_dimension_element_count (self, organization_id):

        return self.__execute_query(self.query_organization_dimension_element_count.format(organization_id))

    def get_query_organization_dimension_count(self, organization_id): 
    
        return self.__execute_query(self.query_organization_dimension_count.format(organization_id))

    def get_query_organization_sth_count(self, organization_id):
        return  self.__execute_query(self.query_organization_sth_count.format(organization_id))
    
    def get_organization_name(self, organization_id):
        results = self.cnx.execute(self.query_organization.format(organization_id)).first()
        return str(results[0])
    
    def get_answers_questionnarie(self, organization_id):
        return self.__execute_query(self.query_answers.format(organization_id))
    
    def get_query_sth_stage(self):
        ''' '''
        return self.__execute_query(self.query_sth)
    
    def get_query_dimension_eye(self):
        ''' '''
        return self.__execute_query(self.query_dimension_eye)
    
    def get_query_element_eye(self):
        ''' '''
        return self.__execute_query(self.query_element_eye)
    
    def get_query_process(self):
        ''' '''
        return self.__execute_query(self.query_process_fcse)
    
    def get_query_sth_from_organization(self, organization_id):
        ''' '''
        return self.__execute_query(self.query_sth_from_organization.format(organization_id))

    def get_query_adopted_level(self):
        ''' '''
        return self.__execute_query(self.query_adopted_level)

    def get_query_pe_organization(self, organization_id):
        ''' '''
        return self.__execute_query(self.query_category_element_organization.format(organization_id))
    
    def get_process_organization(self, organization_id):
        ''' '''
        return self.__execute_query(self.query_process_organization.format(organization_id))
    
    def get_query_category_sth_stage(self):
        ''' '''
        return self.__execute_query(self.query_category_sth)