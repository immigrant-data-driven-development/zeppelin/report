from ctypes import alignment
from turtle import title
from sqlalchemy import column
from cgd.repository import ReportRepository
import pandas as pd 
import plotly.express as px
import plotly.graph_objects as go
import numpy as np

class ReportApl():

    def __init__(self):
        self.repository = ReportRepository()

    def query_organization_process_count(self, organization_id):
        return self.repository.get_query_organization_process_count(organization_id=organization_id)

    def query_percentage_elements_organization(self,organization_id):
        
        print (organization_id)

        return self.repository.get_query_percentage_elements_organization(organization_id=organization_id)
    def query_percentage_eyes_sth_organization(self, organization_id):
        return self.repository.get_query_percentage_eyes_sth_organization(organization_id=organization_id)
    
    def get_query_organization_dimension_element_count (self, organization_id):
        return self.repository.get_query_organization_dimension_element_count(organization_id=organization_id)
    
    def get_query_percentage_eyes_organization(self,organization_id):
        return self.repository.get_query_percentage_eyes_organization(organization_id=organization_id)

    def get_query_organization_dimension_count(self, organization_id):
        return self.repository.get_query_organization_dimension_count(organization_id=organization_id)

    def get_query_organization_sth_count(self, organization_id):
        return self.repository.get_query_organization_sth_count(organization_id=organization_id)

    def get_organization_name(self, organization_id):
        return self.repository.get_organization_name(organization_id=organization_id)

    def __process_return_radar_graph(self, results, group_by, count, column_by, column_r, column_theta, title=None):
        
        results = results.groupby(group_by).count()[count].apply(lambda x: 100 * x / float(x.sum()))
        results.sort_values(by=column_by, inplace=True)
        results.reset_index(inplace=True) 
        fig = px.line_polar(results, r=column_r, theta=column_theta, line_close=True)
        fig.update_traces(fill='toself')
        fig.update_layout(
            height=400,
            title_text = title,
            margin=dict(r=5, l=5, t=40, b=20)
        )
        return fig  


    def __process_result_graph_bar(self,results,columns,columns_count, columns_groupby,columns_index,x,y,color):
        results_count = results[columns]
        # Modificando o nome das colunas
        results_count.columns = columns_count
        results_count = results_count.groupby(columns_groupby).count()[columns_index].apply(lambda x: 100 * x / float(x.sum()))
        results_count.reset_index(inplace=True)
        fig = px.bar(results_count, x=x, y=y,color=color, text_auto=True)
        return fig

    def __process_result_table(self,results, columns, value_1, value_2, value_1_1, value_2_1, size, text_title=None):

        # Selecionando as colunas para a analise
        results_count = results[columns]

        data = {value_1_1: list(results_count[value_1]),
                value_2_1: list(results_count[value_2])}
        # Definindo as colunas da tabela    
        table = pd.DataFrame(data, columns = [value_1_1, value_2_1])

        fig = go.Figure(data=[go.Table(header=dict(values=table.columns),
            cells=dict(values=[table[value_1_1], table[value_2_1]],
                     align=['left', 'left'],
                     font_size=12)
                     )])

        fig.update_layout(
            height=size,
            title_text=text_title,
            margin=dict(r=5, l=5, t=25, b=5)
        )
        
        return fig 
    
    def __processo_result_pivot_table(self, result, columns, columns_count, columns_by, columns_groupby, counts, values_pivot_table, index_pivot_table, colum_pivot_table):

        results_count = result[columns]

        # Modificando o nome das colunas
        results_count.columns = columns_count 
        
        results_count = results_count.groupby(columns_groupby).count()[counts]

        results_count.sort_values(by=columns_by, inplace=True)

        results_count.reset_index(inplace=True)
        
        table = pd.pivot_table(results_count, 
                                values=values_pivot_table, 
                                index=index_pivot_table,
                                aggfunc=np.sum,
                                fill_value=0,
                                margins=True)
        
        if colum_pivot_table is not None:
            table = pd.pivot_table(results_count, 
                                values=values_pivot_table, 
                                index=index_pivot_table,
                                columns=colum_pivot_table, 
                                aggfunc=np.sum,
                                fill_value=0,
                                margins=True)
        table.reset_index(inplace=True)
        
        return table

    def __config_pivot_table(self, result,  column, column_count, column_groupby, counts, columns_by, values_pivot_table , index_pivot_table, colum_pivot_table):
       
       return self.__processo_result_pivot_table(
            result=result, 
            columns =column,
            columns_count = column_count,
            columns_groupby =column_groupby,
            counts = counts,
            columns_by = columns_by,
            values_pivot_table =values_pivot_table,
            index_pivot_table = index_pivot_table,
            colum_pivot_table = colum_pivot_table
        )

    def get_query_percentage_sth_organization(self,organization_id):
        return self.repository.get_query_percentage_sth_organization(organization_id)
        
    def get_query_sth_percetage_organization_table(self, organization_id):

        result = self.repository.get_query_percentage_sth_organization(organization_id)
        return self.__process_result_table(
            results=result, 
            columns=["sth_stage", "percentage"],
            value_1="sth_stage", 
            value_2="percentage", 
            value_1_1="Estágios do StH", 
            value_2_1="Percentual de adoção de práticas (%)", 
            text_title= "Tabela 1: Grau de Adoação das práticas em cada estágio do StH",
            size=160)


    def get_query_percentage_elements_organization(self, organization_id):
        return self.repository.get_query_percentage_elements_organization(organization_id=organization_id)

    def get_query_percentage_process_organization(self, organization_id):
        return self.repository.get_query_percentage_process_organization(organization_id=organization_id)

    def get_query_percentage_process_organization_table(self,organization_id):
        result = self.repository.get_query_percentage_process_organization(organization_id=organization_id)
        return self.__process_result_table(
            results=result, 
            columns=["process", "percentage"],
            value_1="process", 
            value_2="percentage", 
            value_1_1="Processo", 
            value_2_1="Percentual de Adoção (%)", 
            size=260)


    def get_query_percentage_elements_organization(self,organization_id):
        result = self.repository.get_query_percentage_elements_organization(organization_id=organization_id)

    def get_query_percentage_elements_organization_table(self, organization_id):
        result = self.repository.get_query_percentage_elements_organization(organization_id=organization_id)
        return self.__process_result_table(
            results=result, 
            columns=["element", "percentage"],
            value_1="element", 
            value_2="percentage", 
            value_1_1="Elementos", 
            value_2_1="Percentual de Adoção (%)", 
            size=540)


    def get_query_sth_stage_table(self):

        result = self.repository.get_query_sth_stage()
        return self.__process_result_table(
            results=result, 
            columns=["name", "description"],
            value_1="name", 
            value_2="description", 
            value_1_1="Estágios", 
            value_2_1="Descrição", 
            size=300)
    
    def get_query_adopted_level_table(self):

        result = self.repository.get_query_adopted_level()
        return self.__process_result_table(
            results=result, 
            columns=['name', 'description'],
            value_1="name", 
            value_2="description", 
            value_1_1="Estágio de Adoção", 
            value_2_1="Descrição", 
            size=200)
    
    def get_query_dimension_eye_table(self):
        result = self.repository.get_query_dimension_eye()
        return self.__process_result_table(
            results= result, 
            columns=["name", "description"],
            value_1="name",
            value_2="description",
            value_1_1= "Categoria", 
            value_2_1= "Descrição", 
            size= 480)
        
    def get_query_element_eye_table(self):
        result = self.repository.get_query_element_eye()
        return self.__process_result_table(
            results= result, 
            columns=["name", "elements"],
            value_1="name",
            value_2="elements",
            value_1_1= "Categorias", 
            value_2_1= "Elementos", 
            size= 250)
       
    
    def get_query_process_table(self):
        result = self.repository.get_query_process()
        return self.__process_result_table(
            results= result, 
            columns=["name", "description"],
            value_1="name",
            value_2="description",
            value_1_1= "Processo", 
            value_2_1= "Descrição", 
            size= 550)
    
    def get_query_sth_from_organization_bar_graph(self, organization_id):
        
        result = self.repository.get_query_percentage_sth_organization(organization_id)
        
        result.reset_index(inplace=True) 
        fig = px.line_polar(result, r='percentage', theta='sth_stage', line_close=True)
        fig.update_traces(fill='toself')
        fig.update_layout(
            height=400,
            title_text = "Figura 1: Percentual de Adoação em cada estágio do StH.",
            margin=dict(r=5, l=5, t=40, b=20)
        )
        return fig

    
    def get_query_percentage_elements_sth_organization(self,organization_id):
        return self.repository.get_query_percentage_elements_sth_organization(organization_id=organization_id)
    
    def get_query_percentage_sth_process_organization_pivot_table(self,organization_id):
        result = self.repository.get_query_percentage_sth_process_organization(organization_id=organization_id)
        result.columns = ['Processo', 'Estágio do StH', 'Percentual de Adoção']
        
        table = result.pivot_table(
            result,
            index=["Processo"],
            columns=['Estágio do StH'],
            fill_value=0,)

        return table

        
    
    def get_query_percentage_elements_sth_organization_pivot_table(self,organization_id):
        result = self.repository.get_query_percentage_elements_sth_organization(organization_id=organization_id)
        result.columns = ['Estágio do StH', 'Elementos', 'Percentual de Adoção']
        
        table = result.pivot_table(
            result,
            index=["Elementos"],
            columns=['Estágio do StH'],
            fill_value=0,)

        return table




    def get_query_sth_from_organization_pivot_table(self, organization_id):

        result = self.repository.get_query_sth_from_organization(organization_id)
        
        return self.__config_pivot_table(
            result = result, 
            column =['code', 'adopted_level_name', 'sth_stage'],
            column_count = ['Práticas', 'Níveis de Adoção', 'Estágios do StH'],
            column_groupby =['Níveis de Adoção','Estágios do StH'],
            counts = ['Práticas'],
            columns_by = ['Níveis de Adoção'],
            values_pivot_table =['Práticas'],
            index_pivot_table = ['Estágios do StH'],
            colum_pivot_table = ['Níveis de Adoção']
            )
    

    def get_get_query_percentage_eyes_organization_table(self, organization_id):
        result = self.repository.get_query_percentage_eyes_organization(organization_id=organization_id)
        return self.__process_result_table(
            results= result, 
            columns=["dimension", "value"],
            value_1="dimension",
            value_2="value",
            value_1_1= "Categorias", 
            value_2_1= "Percentual de Adoção (%)", 
            text_title= "Tabela 3: Grau de Adoação das práticas em cada dimensão.",
            size= 250)



    def get_query_adopted_level_dimension_sth_organization_radar_graph(self, organization_id):
        result = self.repository.get_query_percentage_eyes_organization(organization_id=organization_id)
       
        result.reset_index(inplace=True) 
        fig = px.line_polar(result, r='value', theta='dimension', line_close=True)
        fig.update_traces(fill='toself')
        fig.update_layout(
            height=400,
            title_text = "Figura 2: Gráfico de radar das práticas em relação ao Practitioners’ Eye.",
            margin=dict(r=5, l=5, t=40, b=20)
        )
        return fig  


    def query_percentage_eyes_sth_organization_pivot_table(self, organization_id):
        
        result = self.repository.get_query_percentage_eyes_sth_organization(organization_id=organization_id)
        result.columns = ['Categoria', 'Estágio do StH', "Percentual de Adoção"]
        
        table = result.pivot_table(
            result,
            index=["Categoria"],
            columns=['Estágio do StH'],
            fill_value=0,)

        return table

    def get_query_adopted_level_dimension_sth_organization_pivot_table(self,organization_id):
        result = self.repository.get_query_pe_organization(organization_id=organization_id)
        return self.__config_pivot_table(
            result = result, 
            column =['code', 'adopted_level_name', 'category', 'sth_stage'],
            column_count = ['Práticas', 'Nível de Adoção', 'Categorias', 'Estágio do StH'],
            column_groupby = ['Nível de Adoção','Categorias', 'Estágio do StH'],
            counts = ['Práticas'],
            columns_by = ['Estágio do StH'],
            values_pivot_table =['Práticas'],
            index_pivot_table = ['Categorias'],
            colum_pivot_table = ['Estágio do StH']
            )

     
    def get_query_adopted_level_dimension_element_sth_organization_pivot_table(self, organization_id):
        result = self.repository.get_query_pe_organization(organization_id=organization_id)
        return result 
        
       
    def get_answers_questionnarie(self,organization_id):
        results = self.repository.get_answers_questionnarie(organization_id=organization_id)
        results = results[['code', 'adopted_level_name', 'statement', 'answer_comment']]
        results.columns = ['Codigo', 'Nível de Adoção', 'Assertiva','Comentário']
        return results

    def get_query_sth_process_table(self, organization_id):
        results = self.repository.get_process_organization(organization_id)
        # Selecionando as colunas para a analise
        results_count = results[['code', 'adopted_level_name', 'process', 'sth_stage']]
        # Modificando o nome das colunas
        results_count.columns = ['Assertives', 'Adopted Level', 'Process','StH Stage']
        results_count = results_count.groupby(['Adopted Level','Process','StH Stage']).count()[['Assertives']]
        results_count.sort_values(by=['StH Stage'], inplace=True)
        table = pd.pivot_table(results_count, values='Assertives', index=['Process'],
                            columns=['StH Stage'], aggfunc=[np.sum],fill_value=0)

        table.reset_index(inplace=True) 
        return table
    
    def get_query_process_organization_radar_graph(self, organization_id):
        result = self.repository.get_query_percentage_process_organization(organization_id=organization_id)
        
        result.reset_index(inplace=True) 
        fig = px.line_polar(result, r='percentage', theta='process', line_close=True)
        fig.update_traces(fill='toself')
        fig.update_layout(
            height=400,
            title_text = "Figura 3: Gráfico de radar dos processos.",
            margin=dict(r=5, l=5, t=40, b=20)
        )
        return fig 
      