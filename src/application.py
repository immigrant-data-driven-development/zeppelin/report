from cgi.report_cgi import ReportCGI
from flask import Flask, send_file
import os
app = Flask(__name__)

@app.route('/report/<organization_id>')
def report(organization_id):
    report_cgi = ReportCGI(organization_id=organization_id)
    report_cgi.create_report()
    file_path = "/app/report/report-{}.html".format(organization_id)
    return send_file(file_path, as_attachment=True)

@app.route('/')
def main():
    return "ok"

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
