from .section import Section

class Section7(Section):

    def create(self):

        self.__create_section_title()
        
        return self.section

        
    def __create_section_title(self):
        title = '## 7. Referências'
        secao_7_reference_1 = '1 . H. H. Olsson, H. Alahyari, and J. Bosch, [“Climbing the" Stairway to Heaven"--A Mulitiple-Case Study Exploring Barriers in the Transition from Agile Development towards Continuous Deployment of Software"](https://ieeexplore.ieee.org/document/6328180), in 2012 38th euromicro conference on software engineering and advanced applications, 2012, pp. 392–399.'
        secao_7_reference_2 = '2 . J. O. Johanssen, A. Kleebaum, B. Paech, and B. Bruegge, [“Practitioners’ eye on continuous software engineering: An interview study”](https://dl.acm.org/doi/10.1145/3202710.3203150), ACM Int. Conf. Proceeding Ser., pp. 41–50, 2018, doi: 10.1145/3202710.3203150.'
        secao_7_reference_3 = '3 . M. P. Barcellos, ["Towards a Framework for Continuous Software Engineering"](https://dl.acm.org/doi/10.1145/3422392.3422469), in SBES 2020 - XXXIV Simpósio Brasileiro de Engenharia de Software, 2020'
        secao_7_reference_4 = '4 . P. S. Santos Júnior, M. P. Barcellos, and F. B. Ruy, [Tell me: Am I going to Heaven? A Diagnosis Instrument of Continuous Software Engineering Practices Adoption](https://dl.acm.org/doi/10.1145/3463274.3463324). In Evaluation and Assessment in Software Engineering (EASE 2021). Association for Computing Machinery, New York, NY, USA, 30–39. https://doi.org/10.1145/3463274.3463324'

        self.section.append(title)
        self.section.append(secao_7_reference_1)
        self.section.append(secao_7_reference_2)
        self.section.append(secao_7_reference_3)
        self.section.append(secao_7_reference_4)
    