from .section import Section

class Section8(Section):

    def create(self):

        self.__create_section_title()
        
        return self.section

    def __create_section_title(self):
        title = '## 8. Respostas'
        answsers = self.application.get_answers_questionnarie(self.organization_id)
        self.section.append(title)
        self.section.append(answsers)
        