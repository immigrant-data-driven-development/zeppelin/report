from cgt.report_apl import ReportApl

class Section ():

   def __init__(self,organization_id):
      self.section = []
      self.organization_id = organization_id
      self.application = ReportApl()

   def create(self):
      pass
   
   
   def process_result_x(self, function, organization_id,rotulo):
      results = function(organization_id=organization_id)
      analysis = []
      for index, row in results.iterrows():
         analysis.append (row[rotulo])
         
      return analysis      

   def process_result(self, function, organization_id, group_by, sum_filed):
      results = function(organization_id=organization_id)
      results = results.groupby(group_by).sum()[sum_filed].sort_values(ascending = True)
        
      analysis = []
        
      for key in results.to_dict().keys():
         analysis.append (key)
      
      return analysis

   def process_result_median(self, function, organization_id, group_by, sum_filed, filter=None):
      
      results = function(organization_id=organization_id)
      
      results = results.groupby(group_by).median()[sum_filed].sort_values(ascending = False)
      
      if filter is not None:
         results = results[filter]
      
      analysis = []
        
      for key in results.to_dict().keys():
         analysis.append (key)
      
      return analysis