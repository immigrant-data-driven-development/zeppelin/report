from .section import Section

class Section1(Section):

  
    def create(self):
        
        titulo = "# Diagnóstico de Adoção de Práticas de Engenharia de Software Contínua Organização "
        nome = self.application.get_organization_name(self.organization_id)
        nome_empresa = '# Empresa: **'+nome+'**'
        
        title_section_1 = '## 1. Objetivo'
        objetivo_paragrafo_1 = 'Este documento tem como objetivo apresentar uma visão geral da adoção de práticas de Engenharia de Software Contínua (ESC) na organização '+nome+'., com base nas respostas dadas pelo representante da organização às questões presentes no instrumento de diagnóstico Zeppelin [4]. As respostas foram fornecidas no contexto de um estudo sobre adoção de práticas de ESC em organizações brasileiras.'
        objetivo_paragrafo_2 = 'Engenharia de Software Contínua é um tópico recente da Engenharia de Software que entende os processos relacionados ao ciclo de vida de software como processos iterativos, integrados e contínuos, permitindo o alinhamento entre os diferentes processos e a evolução contínua da organização a partir da integração dos processos e dados de suas diferentes áreas (Negócio, Desenvolvimento e Operação). Dessa forma, visa ao desenvolvimento iterativo e integrado de software, alinhado ao negócio e orientado a dados.'
        objetivo_paragrafo_3 = 'A partir dos dados informados pelo representante da organização, foi produzida uma visão panorâmica e preliminar da adoção de práticas de ESC na organização. Esse panorama, permitirá à organização vislumbrar práticas que deseja fortalecer ou novas práticas que deseja adotar no futuro para alcançar um ambiente de Engenharia de Software Contínua. É importante ressaltar que este diagnóstico não tem como propósito definir um nível de maturidade para a organização, como ocorre em avaliações considerando modelos tais como o [CMMI](https://cmmiinstitute.com) (Capability Maturity Model) e [MPS.BR](https://softex.br/mpsbr/) (Melhoria de Processo de Software Brasileiro). Esta avaliação considerou práticas necessárias em um ambiente de Engenharia de Software Contínua que, embora sejam organizadas em estágios em alguns modelos, estes não necessariamente caracterizam nível de maturidade.'
        objetivo_paragrafo_4 = 'Para apresentar o panorama de adoção de práticas de ESC na organização, elas foram classificadas considerando-se três diferentes perspectivas, dadas por três modelos que abordam ESC: Stairway to Heaven - StH [1], que considera que práticas de ESC ocorrem em quatro estágios; Practitioners’ Eye - PE [2], que define nove categorias para as práticas de ESC, e o Continuous Software Engineering Framework - CSEF [3], que define dez processos que contêm práticas relevantes em um ambiente de ESC.'
        objetivo_paragrafo_5 = 'A seção 2 deste documento apresenta alguns conceitos utilizados no estudo. As seções 3, 4 e 5 apresentam o panorama geral da empresa em relação, respectivamente, ao Stairway to Heaven - StH [1], ao Practitioners’ Eye – PE [2] e ao Continuous Software Engineering Framework - CSEF [3]. A seção 6 apresenta algumas considerações finais.'
        
        
        self.section.append(titulo)
        self.section.append(nome_empresa)
        self.section.append(title_section_1)
        self.section.append(objetivo_paragrafo_1)
        self.section.append(objetivo_paragrafo_2)
        self.section.append(objetivo_paragrafo_3)
        self.section.append(objetivo_paragrafo_4)
        self.section.append(objetivo_paragrafo_5)
        
        return self.section