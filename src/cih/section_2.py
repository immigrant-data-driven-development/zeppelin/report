from .section import Section

class Section2(Section):

    
    def create(self):

        self.__create_section_title()
        self.__create_section_2_1()
        self.__create_section_2_2()
        self.__create_section_2_3()
        self.__create_section_2_4()
        return self.section
    
    def __create_section_title(self):
        secao_2 = '## 2. Conceitos evolvidos no estudo'
        secao_2_paragrafo_1 = 'Nesta seção são apresentados alguns conceitos utilizados no estudo e que são necessários para um melhor entendimento do panorama apresentado nas seções seguintes. '

        self.section.append(secao_2)
        self.section.append(secao_2_paragrafo_1)
        

    def __create_section_2_1(self):
        secao_2_1 = '### 2.1 Estágios de adoção'
        secao_2_1_paragrafo_1 = 'Para analisar as práticas adotadas pela organização, foram considerados os seguintes graus de adoção:'
        
        estagio_adocao_table = self.application.get_query_adopted_level_table()

        self.section.append(secao_2_1)
        self.section.append(secao_2_1_paragrafo_1)
        self.section.append(estagio_adocao_table)
        
    def __create_section_2_2(self):
        secao_2_2 = '### 2.2 Estágios do StH'
        secao_2_2_paragrafo_1 = 'Stairway to Heaven (StH) [1] é um modelo que descreve o caminho evolutivo que as organizações costumam percorrer para implementar práticas de Engenharia de Software Contínua. O modelo prescreve cinco estágios: '

        estagio_sth_table = self.application.get_query_sth_stage_table()
        
        self.section.append(secao_2_2)
        self.section.append(secao_2_2_paragrafo_1)
        self.section.append(estagio_sth_table)
    
    def __create_section_2_3(self):

        secao_2_3 = '### 2.3 Categorias e Elementos do Practitioners’ Eye'
        secao_2_3_paragrafo_1 = 'Practitioners’ Eye (PE) [2] apresenta um conjunto de categorias que agrupam elementos (práticas) presentes em um ambiente de Engenharia de Software Contínua. Dimensões consideradas: '

        dimensoes_eye_table = self.application.get_query_dimension_eye_table()
        
        secao_2_3_paragrafo_2 = 'Elementos presentes em cada categoria: '
        
        categoria_eye_table = self.application.get_query_element_eye_table()
        
        self.section.append(secao_2_3)
        self.section.append(secao_2_3_paragrafo_1)
        self.section.append(dimensoes_eye_table)
        self.section.append(secao_2_3_paragrafo_2)
        self.section.append(categoria_eye_table)
    
    def __create_section_2_4(self):
        
        secao_2_4 = '### 2.4 Framework para Engenharia de Software Contínua'
        secao_2_4_paragrafo_1 = 'O Framework para Engenharia de Software Contínua (FESC) [3] é inclui dez processos considerados relevantes na implementação de um ambiente de Engenharia de Software Contínua:'

        processo_fce_table = self.application.get_query_process_table()
        secao_2_4_paragrafo_2 = 'A seguir, nas seções 3, 4 e 5, são apresentadas algumas informações sobre as práticas adotadas na organização quando analisadas em relação, respectivamente, ao StH, PE e FESC.'
        secao_2_4_paragrafo_3 = 'As informações apresentadas a seguir foram obtidas a partir dos dados fornecidos pelo representante da organização no formulário por ele respondido. Vale ressaltar que as informações apresentadas a seguir visam fornecer uma visão geral da adoção das práticas na organização como um todo e, assim, não incluem análise individual das práticas que foram investigadas.'

        self.section.append(secao_2_4)
        self.section.append(secao_2_4_paragrafo_1)
        self.section.append(processo_fce_table)
        self.section.append(secao_2_4_paragrafo_2)
        self.section.append(secao_2_4_paragrafo_3)

        