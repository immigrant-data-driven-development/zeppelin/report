from .section import Section

class Section4(Section):

    def create(self):

        self.__create_section_title()
        self.__create_dimension_percentage_table()
        self.__create_paragrafo_2() 

        self.__create_radar_graph()

        self.__create_paragrafo_3()
        self.__create_percentage_eye_sth_table()
        
        self.__create_paragrafo_4()
        self.__creat_dimension_per_element_percentage_table()

        return self.section

        
    def __create_section_title(self):
        title = '## 4. Adoção de práticas de ESC considerando-se PE'
        section_4_paragraf_1 = "Ao se considerar as categorias e elementos de Practitioners’Eye é possível apresentar um panorama da organização sob outra perspectiva, complementar à dos estágios do StH. "
        section_4_paragraf_1 = section_4_paragraf_1 + " A Tabela 3 apresenta as práticas de ESC adotadas na organização, quando analisadas à luz das categorias de PE."
        self.section.append(title)
        self.section.append(section_4_paragraf_1)
    
    def __create_paragrafo_2(self):

        self.dimensions = self.process_result_x(self.application.get_query_percentage_eyes_organization,self.organization_id,'dimension')
         
        section_2_paragrafo_2 = "Quando analisadas à luz das categorias de PE,"  
        section_2_paragrafo_2 = section_2_paragrafo_2 +" essas práticas estão em sua grande parte concentradas nas categorias **{}**, **{}** e **{}** . ".format(self.dimensions[0],self.dimensions[1],self.dimensions[2])
        section_2_paragrafo_2 = section_2_paragrafo_2 +" Percebe-se, assim, que há espaço para aprendizagem e crescimento organizacional em **{}**, **{}**, **{}**, **{}** e **{}**.".format(self.dimensions[-1],self.dimensions[-2],self.dimensions[-3],self.dimensions[-4],self.dimensions[-5])
        section_2_paragrafo_2 = section_2_paragrafo_2 +" O crescimento nessas áreas pode auxiliar ou até mesmo potencializar práticas em outras categorias. A Figura 2 apresenta o gráfico de radar das práticas de ESC adotadas na organização, quando analisadas à luz das categorias de PE. Dessa forma, torna-se possivel observar as categorias no qual a organização possui práticas adotadas."
        
        self.section.append(section_2_paragrafo_2)

    def __create_dimension_percentage_table(self):
        table = self.application.get_get_query_percentage_eyes_organization_table(self.organization_id)
        self.section.append(table)
    
    def __create_radar_graph(self):
        
        radar_graph = self.application.get_query_adopted_level_dimension_sth_organization_radar_graph(self.organization_id)
        self.section.append(radar_graph)
    
    def __create_percentage_eye_sth_table(self):

        table = self.application.query_percentage_eyes_sth_organization_pivot_table(self.organization_id)
        self.section.append(table)
        self.section.append("Tabela 4: Grau de Adoção das pŕaticas por Categoria do Practitioners’Eye e Estágio do StH.")
    
    def __create_paragrafo_3(self):

        sth_stage = self.process_result_median(self.application.query_percentage_eyes_sth_organization,self.organization_id, "sth_stage", "value")

        sth_stage_dimension = self.process_result_median(self.application.query_percentage_eyes_sth_organization,self.organization_id, ["sth_stage", "dimension"], "value")

        paragrafo = "A Tabela 4 apresenta o percentual de adoção de práticas de ESC considerando-se cada categoria de PE e também os estágios StH." 
        paragrafo = paragrafo +  " Observa-se que as capacidades da organização estão mais concentradas nos estágios **{}** e **{}** e em categorias ligadas as práticas de **{}** e **{}**.".format(sth_stage[0],sth_stage[1],sth_stage_dimension[1][1],sth_stage_dimension[2][1]) 
        paragrafo = paragrafo +" Processos e boas práticas ligadas às categorias **{}**, **{}**, **{}** e **{}** são pouco expressivas na organização.".format(self.dimensions[-1],self.dimensions[-2],self.dimensions[-3],self.dimensions[-4])
        self.section.append (paragrafo)
    
    
    def __create_paragrafo_4(self):
        analysis = self.process_result_x(self.application.query_percentage_elements_organization,self.organization_id,'element')

        paragrafo = "A Tabela 5 apresenta o percentual de adoção de práticas de ESC considerando-se os elementos das categorias de PE."
        paragrafo = paragrafo +" Observa-se que a organização possui boas habilidades no que tange **{}**, **{}** e **{}**.".format(analysis[0],analysis[1], analysis[2])
        paragrafo = paragrafo +" Processos e boas práticas ligadas aos elementos **{}**, **{}** e **{}** são pouco expressivas na organização.".format(analysis[-1],analysis[-2], analysis[-3])

        self.section.append (paragrafo) 
        
    def __creat_dimension_per_element_percentage_table(self):
        
        table = self.application.get_query_percentage_elements_organization_table(self.organization_id)
        
        self.section.append(table)
        self.section.append("Tabela 5: Grau de Adoção das pŕaticas por Elemento do Practitioners’Eye.")
        
        
        sth_stage = self.process_result_median(self.application.get_query_percentage_elements_sth_organization,self.organization_id, "sth_stage", "percentage")
        sth_stage_filter = []
        
        sth_stage_filter.append(sth_stage[0])
        sth_stage_filter.append(sth_stage[1])

        sth_stage_dimension = self.process_result_median(self.application.get_query_percentage_elements_sth_organization,self.organization_id, ["sth_stage", "element"], "percentage",sth_stage_filter)

        paragrafo = "A Tabela 6 apresenta o percentual de adoção de práticas de ESC considerando-se cada Elemento de PE e também os estágios StH." 
        paragrafo = paragrafo +  " Observa-se que as capacidades da organização estão mais concentradas nos estágios **{}** e **{}** e em categorias ligadas as práticas de **{}** e **{}**.".format(sth_stage[0],sth_stage[1],sth_stage_dimension[-1][-1],sth_stage_dimension[-2][-1]) 
        self.section.append (paragrafo)

        pivot_table = self.application.get_query_percentage_elements_sth_organization_pivot_table(self.organization_id)    
        
        self.section.append(pivot_table)
        self.section.append("Tabela 6: Grau de Adoção das pŕaticas por Elemento do Practitioners’Eye e Estágio do StH")