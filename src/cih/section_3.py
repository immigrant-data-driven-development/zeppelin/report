from .section import Section

class Section3(Section):

    def create(self):

        self.__create_section_title()
        self.__create_section_1()
        self.__create_get_query_sth_percetage_organization_table()
        self.__create_get_query_sth_from_organization_bar_graph()
        self.__create_section_2()
        self.__create_get_query_sth_from_organization_table()
        return self.section
    
    def __create_section_title(self):
        title = '## 3. Adoção de práticas de ESC considerando-se os estágios do StH'
        
        self.section.append(title)
    
    def __create_section_2(self):
        section_2_paragrafo_2 = "A Tabela 2 apresenta a quantidade de práticas por nível de adoção e estágios do StH. A"
        #section_2_paragrafo_2 = section_2_paragrafo_2 + " Pode-se observar que a organização **nenhuma** prática em **Nível de Processo ou Abandonado**;"
        #section_2_paragrafo_2 = section_2_paragrafo_2 + " e poucas práticas no **Nível de Organização**. Dessa forma, isso pode ser um indicador de que a organização possui pouca formalização de seus processos e boas práticas." 
        #section_2_paragrafo_2 = section_2_paragrafo_2 + " Como consequência, é provável que o **conhecimento organizacional** esteja centrado no conhecimento tácito dos membros da organização." 
        #section_2_paragrafo_2 = section_2_paragrafo_2 + " Mesmo as práticas presentes no **Nível de Organização** possuem poucos artefatos que comprovem que há documentação ou processos institucionalizados para tais práticas."
        
        self.section.append(section_2_paragrafo_2)

    def __create_section_1(self):

        sth_values = self.process_result(self.application.get_query_organization_sth_count,self.organization_id, "sth_stage", "qtd")
        
        secao_3_1_paragrafo_1 = "A Tabela 1 e a Figura 1 apresenta o perfil de adoção de práticas pela organização considerando-se os estágios do modelo StH. "
        secao_3_1_paragrafo_1 = secao_3_1_paragrafo_1 + " Como pode ser observado, a organização possui uma concentração maior de suas"
        secao_3_1_paragrafo_1 = secao_3_1_paragrafo_1 + "capacidades em práticas dos estágios **{}** e **{}**. ".format(sth_values[3], sth_values[2])
        secao_3_1_paragrafo_1 = secao_3_1_paragrafo_1 + "Em relação aos estágios **{}** e **{}** há, ".format(sth_values[1], sth_values[0])
        secao_3_1_paragrafo_1 = secao_3_1_paragrafo_1 + " ainda, várias práticas que não são adotadas e poucas institucionalizadas na "
        secao_3_1_paragrafo_1 = secao_3_1_paragrafo_1 + " organização como um todo. Isso indica que ainda há espaço para aprendizagem na "
        secao_3_1_paragrafo_1 = secao_3_1_paragrafo_1 + " organização para práticas relacionadas a esses dois estágios do StH."
    
        self.section.append(secao_3_1_paragrafo_1)
        
        secao_3_1_paragrafo_1= "Ressalta-se que, dependendo do modelo de negócio da organização, a não adoção de algumas práticas não implica que elas devam ser adotadas no futuro. Por exemplo, se a organização não desenvolve um produto no qual seja pertinente a realização de experimentação automática para coleta de feedback dos usuários, a não realização de práticas de experimentação contínua pode não indicar uma limitação da organização, mas sim uma decisão alinhada a seu modelo de negócio."
        
        self.section.append(secao_3_1_paragrafo_1)
    
    
    def __create_get_query_sth_percetage_organization_table(self):
        secao_3_1_grafico = self.application.get_query_sth_percetage_organization_table(organization_id=self.organization_id)
        self.section.append(secao_3_1_grafico)
    
    
    def __create_get_query_sth_from_organization_bar_graph(self):
        secao_3_1_grafico = self.application.get_query_sth_from_organization_bar_graph(organization_id=self.organization_id)
        self.section.append(secao_3_1_grafico)
        

    def __create_get_query_sth_from_organization_table(self):
        secao_3_1_table = self.application.get_query_sth_from_organization_pivot_table(organization_id=self.organization_id)
        self.section.append(secao_3_1_table)
        self.section.append("Tabela 2: Relação entre os níveis de adoção por estágio do StH.")
    