from .section import Section

class Section6(Section):

    def create(self):

        self.__create_section_title()
        self.__create_paragrafo_1()
        self.__create_paragrafo_2()
        
        return self.section

        
    def __create_section_title(self):
        title = '## 6. Conclusões e Reflexões'
        self.section.append(title)
       
    def __create_paragrafo_1(self):
        paragrafo = "Este documento apresentou uma visão consolidada de informações fornecidas por um representante  da organização que participou do estudo respondendo o questionário sobre adoção de práticas relacionadas a Engenharia de Software Contínua."
        self.section.append(paragrafo)
        self.section.append("Esta é uma visão panorâmica da adoção das práticas pela organização e não deve ser considerada como instrumento de nível de maturidade da organização em relação às práticas investigadas.")
    
    def __create_paragrafo_2(self):


        sth_values = self.application.get_query_percentage_sth_organization(self.organization_id)

        paragrafo = " Analisando-se as informações consolidadas, é possível perceber que a organização tem se destacado no desenvolvimento de práticas e processos ligados:\n"
        self.section.append (paragrafo)
        
        paragrafo = "* aos estágios **{}** e **{}** do StH;".format(sth_values['sth_stage'][0],sth_values['sth_stage'][1])
        self.section.append (paragrafo)
        
        
        categorias = self.process_result_median(self.application.query_percentage_eyes_sth_organization,self.organization_id, ["sth_stage", "dimension"], "value")
        elements = self.process_result_median(self.application.get_query_percentage_elements_sth_organization,self.organization_id, ["sth_stage", "element"], "percentage")

        paragrafo = "* às categorias **{}**  e **{}** e os elementos **{}** e **{}** de PE;".format(categorias[1][1],categorias[2][1],elements[1][1],elements[2][1])
        
        self.section.append (paragrafo)
        
        analysis = self.process_result(self.application.get_query_percentage_process_organization,self.organization_id, ["process"], "percentage")
        
        paragrafo = "* aos processos **{}**, **{}**, **{}** e **{}** de FESC.".format(analysis[-1],analysis[-2],analysis[-3],analysis[-4])
        
        self.section.append (paragrafo)
        
        self.section.append("Agradecemos sua participação no estudo e colocamo-nos à disposição caso você tenha dúvidas ou interesse em outras informações.")
        
        
    