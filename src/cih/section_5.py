from .section import Section

class Section5(Section):

    def create(self):

        self.__create_section_title()
        self.__create_paragrafo_1()
        self.__create_table()
        self.__create_radar_graph()
        self.__create_process_sth_pivot_table()
        
        return self.section

        
    def __create_section_title(self):
        title = '## 5. Adoção em relação aos Processos'
        self.section.append(title)
       
    def __create_paragrafo_1(self):


        analysis = self.process_result(self.application.get_query_percentage_process_organization,self.organization_id, ["process"], "percentage")

        paragrafo = "A Tabela 7 apresenta as práticas de ES adotadas na organização, quando analisadas à luz dos processos do FESC. "
        paragrafo = paragrafo + " Como pode ser observado, a organização possui mais força nas práticas associadas aos processos **{}** e **{}**.".format(analysis[-1], analysis[-2])
        paragrafo = paragrafo + " No entanto, a organização possui poucas práticas ligadas ao **{}**, **{}** e **{}**.".format(analysis[0], analysis[1], analysis[2])
        self.section.append(paragrafo)
    
    def __create_table(self):
        table = self.application.get_query_percentage_process_organization_table(self.organization_id)
        self.section.append(table)
        self.section.append("Tabela 7: Percentual de adoção por processos.")
    

    def __create_radar_graph(self):
        self.section.append("A Figura 3 apresenta o gráfico de radar das práticas de ESC adotadas na organização, quando analisadas à luz dos processos. Dessa forma, torna-se possivel observar os processos no qual a organização possui mais práticas adotadas.")
        radar = self.application.get_query_process_organization_radar_graph(self.organization_id)
        self.section.append(radar)
    
    def __create_process_sth_pivot_table(self):
        paragrafo = "A Tabela 8 apresenta o percentual das práticas aplicadas no processo do FCE adotadas na organização, quando analisadas à luz dos Estágio do StH."
        self.section.append(paragrafo)  
        pivot_table = self.application.get_query_percentage_sth_process_organization_pivot_table(self.organization_id)
        self.section.append(pivot_table)
        self.section.append("Tabela 8: Relação entre Processos  do FCSE e Estágio do StH.")
    
    
    