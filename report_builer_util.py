# Lib to support gap analysis
#Tutoriais
# https://www.datacamp.com/community/tutorials/histograms-matplotlib?utm_source=adwords_ppc&utm_campaignid=1455363063&utm_adgroupid=65083631748&utm_device=c&utm_keyword=&utm_matchtype=b&utm_network=g&utm_adpostion=&utm_creative=332602034358&utm_targetid=dsa-429603003980&utm_loc_interest_ms=&utm_loc_physical_ms=1001547&gclid=CjwKCAjwnK36BRBVEiwAsMT8WFpQ8Vnhx1pxEpNIA4JNvbohkL0g9QgmdRq3cgNBAqNVbDN3uCsy7BoCRYsQAvD_BwE

# dataSet
## https://data36.com/pandas-tutorial-2-aggregation-and-grouping/

from decouple import config

class AnalysisQuestionnarieUtil():
    
    def __init__(self):

        DB_HOST = config('DB_HOST')
        DB_USERNAME = config('DB_USERNAME')
        DB_PASSWORD = config('DB_PASSWORD')
        DB_PORT = config('DB_PORT')
        DB_NAME = config('DB_NAME')

        self.DATABASE = '''postgresql://{}:{}@{}:{}/{}'''.format(DB_USERNAME,DB_PASSWORD,DB_HOST,DB_PORT,DB_NAME)
        
        self.query_sth_from_organization = "select distinct code, adopted_level_name, sth_stage, adopted_level_percentage from questionnaire_answer_view order by sth_stage"

        self.query_category_element_organization = "select distinct code, adopted_level_name, sth_stage, category, element , adopted_level_percentage from questionnaire_answer_view order by sth_stage"

        self.query_process_organization = "select * from questionnaire_answer_view order by sth_stage"
       

        self.query_category_sth = "select distinct code, element, category, sth_stage from assertive_category_stage_process order by category"

       
        #### Novo
        self.query_adopted_level = "select name, description from questionnaire_adoptedlevel qa order by percentage "

        self.query_sth = "select name, description from sth_stage ss order by name"

        self.query_dimension_eye = "select name, description from practitioners_eye_dimension ped order by name"

        self.query_element_eye = "select  ped.name, string_agg (pee.name, ', ') as elements from practitioners_eye_element pee inner join practitioners_eye_dimension ped on pee.dimension_id = ped.id group by ped.name order by ped.name"

        self.query_process_fcse = "select distinct name, description from cse_framework_process cfp order by name"

    def gap_analysis_database(self):
        return self.DATABASE

    def get_query_sth_stage(self):
        return self.query_sth
    
    def get_query_dimension_eye(self):
        return self.query_dimension_eye
    
    def get_query_element_eye(self):
        return self.query_element_eye
    
    def get_query_process(self):
        return self.query_process_fcse

    ###########    
    
    def get_query_sth_from_organization(self):
        return self.query_sth_from_organization

    def get_query_adopted_level(self):
        return self.query_adopted_level

    def get_query_pe_organization(self):
        return self.query_category_element_organization
    
    def get_process_organization(self):
        return self.query_process_organization
    
    def get_query_category_sth_stage(self):
        return self.query_category_sth