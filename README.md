# Report Builder

Creates a report based on Zeppelin`s database.

## Install

```bash
pip install -r requirements.txt
```

## Usage

Configure Environment Variable on the file .env

```bash

docker compose up --build 

```

Open a browser in [http://localhost:5000/report/<organizational_id>](http://localhost:5000/report/<organizational_id>).

Replace <organization_id> by a organization's id on Zeppelin' service. 
